//index.js
//获取应用实例
const app = getApp()
Page({
	data: {
		userName : '' , 
		disabled : true , 
	} , 
	onLoad: function() {
		app.openConnect(app.globalData.host , app.globalData.port , null , this);
	} , 
	onShow:function(){
		if (app.globalData.userName) {
			wx.navigateTo({
				url : '/pages/chat/index'
			});
			app.globalData.onMessage = [];
			return;
		}
		// 如果是新用户，则删除缓存
		wx.removeStorageSync('list');
	} , 
	changeInput : function(e){
		let name = e.detail.value
		app.globalData.userName = name;
	} , 
	nicknameNotNull : function(){
		wx.showToast({title : '名称不能为空' , icon : 'none'});
	} , 
	// 去聊天室
	toChat : function(){
		if(!app.globalData.userName){
			this.nicknameNotNull();
			return;
		}
		app.post('/login' , {name : app.globalData.userName} , function(res){
			app.sendMsg(app.globalData.userName , 'login');
			wx.navigateTo({
				url : '/pages/chat/index'
			});
		});
		
	}
})