# 小程序聊天室-前端

#### 项目介绍
在小程序中也能有个自己的聊天室
<figure class="half">
<img src="./demo/image-20200813175946139.png" alt="image-20200813175946139" style="width:20%;" />
<img src="./demo/image-20200813180008595.png" alt="image-20200813180008595" style="width:20%;" />
<img src="./demo/image-20200813180131090.png" alt="image-20200813180131090" style="width:20%;" />
<img src="./demo/WX20200814-160657@2x.png" alt="image-20200813180131090" style="width:20%;" />
</figure>
#### 软件架构

小程序 + NodeJs 


#### 安装教程

1. 新建一个小程序，选择下载好的文件夹即可
2. 下载“聊天室后台”，用的NodeJs实现，下载地址：[点我](https://gitee.com/colingit/small_program_chat_room_api)
4. 更改app.js的属性globalData 中的配置，修改连接IP、端口信息
5. 运行服务器，刷新小程序，至此完毕

#### 使用说明

用户输入昵称进入，进入后有系统通知、可以发送表情（默认处理无论表情在左边、右边、文字嵌入，显示都统一在左边），发送普通文本，可二开