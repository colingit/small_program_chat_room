//app.js
App({
	onLaunch: function() {
		for(let items in this.globalData.looks){
			this.globalData.looks[items].forEach((item) => {
				item.url = '../images/looks/' + items + '/' + item.url;
			})
		}
	},
	socketTask : '' , 
	errorCode : 0 , // 错误代码
	interval : 0 , // 定时器标识
	intervalNumber : 0 , // 定时重连的次数
	state : 0 , // 重连状态 0 离线 1 在线 2 重连
	bindMessage : 0 , // 绑定回调
	globalData: {
		userInfo: null , 
		userName : '' , 
		host : '127.0.0.1' , //连接IP
		port : 3000 , //连接端口 
		chatList : [] , //聊天记录
		onMessage : [] , 
		looks : {
			w : [
				{name : 'wx' , url : '01.png'} , 
				{name : 'xk' , url : '02.png'} , 
				{name : 'xk1' , url : '03.png'} , 
				{name : 'lh' , url : '04.png'} , 
				{name : 'ng' , url : '05.png'} , 
				{name : 'kx' , url : '06.png'} , 
				{name : 'hx' , url : '07.png'} , 
				{name : 'ng1' , url : '08.png'} , 
			] , 
			web : [
				{name : 'wx' , url : '01.gif'} , 
				{name : 'xk' , url : '02.jpg'} , 
				{name : 'xk1' , url : '03.jpg'} , 
				{name : 'lh' , url : '04.jpg'} , 
				{name : 'ng' , url : '05.jpg'} , 
				{name : 'kx' , url : '06.jpg'} , 
				{name : 'hx' , url : '07.jpg'} , 
				{name : 'ng1' , url : '08.jpg'} , 
				{name : 'ng12' , url : '09.jpg'} , 
				{name : 'ng13' , url : '10.jpg'} , 
				{name : 'ng11' , url : '11.jpg'} , 
				{name : 'ng11' , url : '12.gif'} , 
				{name : 'ng14' , url : '13.jpg'} , 
				{name : 'ng15' , url : '14.gif'} , 
			]
		} , 
		scope : null , 
	} , 
	// 发送socket消息
	sendMsg : function(msg , type){
		var data = {
			data : msg , 
			type : type , 
			time : this.getTime() , 
			nickname : this.globalData.userName , 
		};
		this.socketTask.send({data : JSON.stringify(data)});
		return data;
	} , 
	// 连接服务器
	openConnect : function(host , port , messageCallback , scope){
		this.scope = scope;
		if(!this.socketTask){
			this.socketTask = wx.connectSocket({
				url: 'ws://' + host + ':' + port
			});
			// socket连接成功
			this.socketTask.onOpen(() => {
				if (this.state == 2) {
					this.state = 1;
					wx.showToast({
						icon : 'none' , 
						mask : true , 
						title : '服务器连接成功' , 
					})
					if (this.interval) {
						clearInterval(this.interval);
					}
				}
				console.log('连接服务器成功！');
				scope && scope.setData({
					disabled : false 
				})
			})
			// socket连接错误
			this.socketTask.onError(this.errorConnect)
			// socket被关闭
			this.socketTask.onClose(this.colseConnect)
		}
		if (!this.bindMessage) {
			// 收到服务器消息
			this.socketTask.onMessage((res) => {
				this.bindMessage = true;
				var data = res.data;
				var parseData = JSON.parse(data);
				if (this.globalData.onMessage.length == 0 && this.globalData.chatList.length == 0) {
					this.globalData.onMessage.push(parseData);
				}else{
					let page = this.getPager();
					page.requestMessage(parseData);
				}
			})
		}
		return this.socketTask;
	} , 
	getPager(){
		let pager = getCurrentPages()
		let page = pager[pager.length - 1]
		return page;
	} , 
	// 连接被关闭
	colseConnect : function(res){
		this.globalData.userName = '';
		this.socketTask = '';
		console.log('WebSocket连接被关闭！')
		console.log(res);
		if (res.code == '1006' && this.errorCode == 0) {
			wx.redirectTo({
				url : '/pages/index/index'
			})
			return;
		}
		this.scope && this.scope.setData({
			disabled : true 
		})
	} , 
	// 连接错误
	errorConnect : function(res){
		this.globalData.userName = '';
		console.log('WebSocket连接打开失败，请检查！')
		console.log(res);
		this.errorCode = 404;
		this.socketTask = '';
		if (this.state == 0) {
			wx.showModal({
				content : '服务器未开启' , 
				confirmText : '尝试重连' , 
				success : (res) => {
					if (res.confirm) {
						this.reconnect();
					}
				}
			})
			this.scope && this.scope.setData({
				disabled : true 
			})
		}
	} , 
	// 重连机制
	reconnect : function(){
		this.state = 2; // 重连
		this.interval = setInterval(() => {
			this.intervalNumber ++;
			if (this.intervalNumber > 100) {
				clearInterval(this.interval);
				return;
			}
			wx.showToast({
				icon : 'none' , 
				mask : true , 
				title : '正在重连第' + this.intervalNumber + '次' , 
				// duration : 1500 ,
			})
			// 定时检查服务器是否可连接
			this.openConnect(this.globalData.host , this.globalData.port , null , this.scope);
		} , 1500);
	} , 
	getTime : function(){
		var myDate = new Date();
		return myDate.toLocaleString();
	} , 
	// 显示消息
	showMsg : function(msg){
		var preg = /(\[([\:a-z0-9]+)\])/g , look = [] , names = [];
		// 匹配数据
		var data = msg.replace(preg , (arg1 , arg2 , arg3 , arg4 , arg5) => {
			let splits = arg3.split(':');
			if (splits[0] in this.globalData.looks) {
				let items = this.globalData.looks[splits[0]]
				items.forEach((item , index) => {
					if(item.name == splits[1]){
						names.push(item.url);
					}
				});
			}
			return '';
		});
		var pushItem = {msg : data};
		if(names.length > 0){
			pushItem.looks = names;
			pushItem.type = 'look';//表明是表情类型
		}
		return pushItem;
	} ,
	// 基础请求
	request : function(url , method , data , success , faild){
		wx.request({
			url : 'http://' + this.globalData.host + ':' + this.globalData.port + url , 
			data : data , 
			method : method , 
			dataType : 'json' , 
			success : function(res){
				let data = res.data
				if (data.code == 200) {
					success(data);
				}else{
					wx.showToast({
						icon : 'none' , 
						mask : true , 
						title : data.msg , 
					})
					return;
				}
			} , 
			fail : function(e){
				console.log(e);
			}
		})
	} , 
	// get请求
	get(url , success , faild){
		this.request(url , 'get' , null , success , faild)
	} , 
	// post请求
	post(url , data , success , faild){
		this.request(url , 'post' , data , success , faild)
	} , 
})